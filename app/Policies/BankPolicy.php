<?php

namespace App\Policies;

use App\User;
use App\Bank;
use Illuminate\Auth\Access\HandlesAuthorization;

class BankPolicy
{
    use HandlesAuthorization;

    public function view(User $user, Bank $bank)
    {
        return $user->hasRole('admin');
    }

    public function create(User $user)
    {
        return $user->hasRole('admin');
    }

    public function update(User $user, Bank $bank)
    {
        return $user->hasRole('admin');
    }

    public function delete(User $user, Bank $bank)
    {
        return $user->hasRole('admin');
    }
}
