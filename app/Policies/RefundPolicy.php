<?php

namespace App\Policies;

use App\User;
use App\Refund;
use Illuminate\Auth\Access\HandlesAuthorization;

class RefundPolicy
{
    use HandlesAuthorization;

    public function view(User $user, Refund $refund)
    {
        return $user->hasRole('admin') || $refund->user_id == $user->id;
    }

    public function create(User $user)
    {
        return true;
    }

    public function update(User $user, Refund $refund)
    {
        return $user->hasRole('admin') || $refund->user_id == $user->id;
    }

    public function delete(User $user, Refund $refund)
    {
        return $user->hasRole('admin') || $refund->user_id == $user->id;
    }
}
