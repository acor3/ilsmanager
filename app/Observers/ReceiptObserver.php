<?php

namespace App\Observers;

use Mail;
use Log;
use DB;

use App\Receipt;

class ReceiptObserver
{
    public function creating(Receipt $receipt)
    {
        list($year, $month, $day) = explode('-', $receipt->date);
        $receipt->number = Receipt::where(DB::raw('YEAR(date)'), $year)->max('number') + 1;
    }
}
