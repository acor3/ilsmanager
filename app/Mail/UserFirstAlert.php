<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserFirstAlert extends Mailable
{
    use Queueable, SerializesModels;

    private $user = null;

    public function __construct($user)
    {
        $this->user = $user;
    }

    public function build()
    {
        return $this->subject('Avviso scadenza quota')->view('email.user_alert_fee', ['user' => $this->user]);
    }
}
