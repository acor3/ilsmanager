<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;

use Mail;
use Log;

use App\Mail\UserRegistered;
use App\User;
use App\Config;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/register/thanks';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function thanks()
    {
        return view('auth.register_pay');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $provinces = join(',', array_map(function($a) {
            return $a[0];
        }, allProvinces()));

        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'surname' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'username' => 'required|string|alpha_dash|max:255|unique:users',
            'birth_date' => 'required|string|max:255',
            'birth_place' => 'required|string|max:255',
            'birth_prov' => 'required|string|in:' . $provinces,
            'taxcode' => 'required|string',
            'address_street' => 'required|string|max:255',
            'address_place' => 'required|string|max:255',
            'address_prov' => 'required|string|in:' . $provinces,
            'statuto' => 'required',
            'privacy' => 'required',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = new User();
        $user->name = ucwords($data['name']);
        $user->surname = ucwords($data['surname']);
        $user->email = strtolower($data['email']);
        $user->username = strtolower($data['username']);
        $user->birth_date = $data['birth_date'];
        $user->birth_place = ucwords($data['birth_place']);
        $user->birth_prov = $data['birth_prov'];
        $user->taxcode = strtoupper($data['taxcode']);
        $user->address_street = ucwords($data['address_street']);
        $user->address_place = ucwords($data['address_place']);
        $user->address_prov = $data['address_prov'];
        $user->password = Hash::make(Str::random(20));
        $user->status = 'pending';
        $user->type = 'regular';
        $user->notes = '';
        $user->section_id = $data['section_id'] ?? 0;
        $user->request_at = date('Y-m-d');
        $user->save();

        if (Config::getConfig('custom_email_aliases') == '1') {
            $user->setConfig('email_behaviour', 'alias');
        }

        try {
            Mail::to(Config::getConfig('association_email'))->send(new UserRegistered($user));
        }
        catch(\Exception $e) {
            Log::error('Impossibile inviare notifica per nuovo utente registrato: ' . $e->getMessage());
        }

        return $user;
    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();
        event(new Registered($user = $this->create($request->all())));
        return $this->registered($request, $user) ?: redirect($this->redirectPath());
    }
}
