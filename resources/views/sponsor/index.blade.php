@extends('layouts.app')

@section('content')
<div class="container">
    @can('create', App\Sponsor::class)
        <div class="row">
            <div class="col-md-12">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createSponsor">Registra Nuovo</button>
                <div class="modal fade" id="createSponsor" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Registra Sponsor</h5>
                                <button type="button" class="close" data-dismiss="modal">
                                    <span>&times;</span>
                                </button>
                            </div>
                            <form method="POST" action="{{ route('sponsor.store') }}">
                                @csrf
                                <div class="modal-body">
                                    @include('sponsor.form', ['object' => null])
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annulla</button>
                                    <button type="submit" class="btn btn-primary">Salva</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="row mt-3">
        <div class="col-md-12">
            <table class="table">
                <thead>
                    <tr>
                        <th>Nome</th>

                        @if($currentuser->hasRole('admin'))
                            <th>Scadenza</th>
                            <th>Modifica</th>
                        @endif
                    </tr>
                </thead>
                <tbody>
                    @foreach($objects as $sponsor)
                        <tr>
                            <td>{{ $sponsor->name }}</td>

                            @if($currentuser->hasRole('admin'))
                                <td>{{ $sponsor->expiration }}</td>
                                <td><a href="{{ route('sponsor.edit', $sponsor->id) }}"><span class="oi oi-pencil"></span></a></td>
                            @endif
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
