@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createUser">Nuovo Rimborso Spese</button>
            <div class="modal fade" id="createUser" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Nuovo Rimborso Spese</h5>
                            <button type="button" class="close" data-dismiss="modal">
                                <span>&times;</span>
                            </button>
                        </div>
                        <form method="POST" action="{{ route('refund.store') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="modal-body">
                                @include('refund.form', ['object' => null])
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Annulla</button>
                                <button type="submit" class="btn btn-primary">Salva</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row mt-3">
        <div class="col-md-12">
            <table class="table">
                <thead>
                    <tr>
			<th>ID</th>
                        <th>Data</th>
                        <th>Ammontare</th>
                        <th>Socio</th>
                        <th>Sezione</th>
                        <th>Dettagli</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($objects as $refund)
                        <tr class="{{ $refund->refunded ? 'table-success' : 'table-danger' }}">
                            <td>{{ $refund->id }}</td>
                            <td>{{ $refund->date }}</td>
                            <td>{{ $refund->amount }}</td>
                            <td>{{ $refund->user ? $refund->user->printable_name : '' }}</td>
                            <td>{{ $refund->section ? $refund->section->city : '' }}</td>
                            <td>
                                <a href="{{ route('refund.edit', $refund->id) }}"><span class="oi oi-pencil"></span></a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
