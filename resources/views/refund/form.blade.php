<div class="form-group row">
    <label for="date" class="col-sm-4 col-form-label">Data</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" name="date" value="{{ $object ? $object->date : date('Y-m-d') }}" required>
    </div>
</div>
<div class="form-group row">
    <label for="amount" class="col-sm-4 col-form-label">Ammontare</label>
    <div class="col-sm-8">
        <input type="number" class="form-control" step="0.01" name="amount" value="{{ $object ? $object->amount : '' }}" required>
    </div>
</div>
@if($currentuser->hasRole('admin'))
    <div class="form-group row">
        <label for="user_id" class="col-sm-4 col-form-label">Utente</label>
        <div class="col-sm-8">
            @include('user.select', ['name' => 'user_id', 'select' => ($object ? $object->user_id : $currentuser->id)])
        </div>
    </div>
@endif
<div class="form-group row">
    <label for="section_id" class="col-sm-4 col-form-label">Sezione Locale</label>
    <div class="col-sm-8">
        @include('section.select', ['name' => 'section_id', 'select' => ($object ? $object->section_id : 0)])
    </div>
</div>
<div class="form-group row multiple-files">
    <label for="file" class="col-sm-4 col-form-label">File</label>
    <div class="col-sm-8">
        @if($object)
            <ul>
                @foreach($object->attachments as $attach)
                    <li><a href="{{ route('refund.download', ['file' => $object->id . '/' . $attach]) }}">{{ basename($attach) }}</a></li>
                @endforeach
            </ul>
        @endif

        <input type="file" name="file[]" autocomplete="off" {{ $object ? '' : 'required' }}>
        <small class="form-text text-muted">Si prega di allegare tutta la documentazione correlata: fatture, scontrini, biglietti...</small>
    </div>
</div>
<div class="form-group row">
    <label for="notes" class="col-sm-4 col-form-label">Note</label>
    <div class="col-sm-8">
        <textarea class="form-control" name="notes" placeholder="Scrivere qui una breve descrizione delle spese affrontate e di come desideri ricevere il rimborso (fornisci un IBAN, ecc.)" required>{{ $object ? $object->notes : '' }}</textarea>
    </div>
</div>
