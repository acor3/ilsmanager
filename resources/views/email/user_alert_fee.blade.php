<p>
    Attenzione! La tua quota di iscrizione a {{ App\Config::getConfig('association_name') }} non risulta pagata!
</p>
<p>
    Ti invitiamo a consultare la tua situazione su {{ route('home') }} (il tuo username, anche per il reset della password, è {{ $user->username }}) e a procedere al versamento delle quote mancanti ad una delle seguenti coordinate:
</p>
<ul>
    @foreach(App\Bank::all() as $bank)
        <li>{{ $bank->name }} - {{ $bank->identifier }}</li>
    @endforeach
</ul>
<p>
    Il tuo account sarà sospeso tra 15 giorni in mancanza del versamento delle quote arretrate. Se non intendi rinnovare l'iscrizione, ignora questa comunicazione e le successive oppure comunicacelo per evitare ulteriori solleciti.
</p>
<p>
    Per eventuali domande o dubbi puoi scrivere all'indirizzo email {{ App\Config::getConfig('association_email') }}
</p>
