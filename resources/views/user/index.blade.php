@extends('layouts.app')

@section('content')
<div class="container">
    @can('create', App\User::class)
        <div class="row">
            <div class="col-md-12">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createUser">Registra Nuovo</button>
                <div class="modal fade" id="createUser" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Registra Socio</h5>
                                <button type="button" class="close" data-dismiss="modal">
                                    <span>&times;</span>
                                </button>
                            </div>
                            <form method="POST" action="{{ route('user.store') }}">
                                @csrf
                                <div class="modal-body">
                                    @include('user.form', ['object' => null])
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annulla</button>
                                    <button type="submit" class="btn btn-primary">Salva</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <button class="btn btn-default show-all" data-target=".table">Mostra Tutti</button>
            </div>
        </div>
    @endif

    <div class="row mt-3">
        <div class="col-md-12">
            <div class="alert alert-info">
                Ci sono attualmente {{ App\User::where('status', 'active')->where('type', 'regular')->count() }} persone e {{ App\User::where('status', 'active')->where('type', 'association')->count() }} associazioni iscritte.
            </div>
        </div>
    </div>

    @if($currentuser->hasRole('admin'))
        @if(App\User::where('status', 'pending')->orderBy('surname')->count() > 0)
            <div class="row mt-3">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">Soci in Attesa di Approvazione</div>

                        <div class="card-body">
                            <form method="POST" action="{{ route('user.approve') }}">
                                @csrf

                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Nome</th>
                                            <th>Cognome</th>
                                            <th>Nickname</th>
                                            <th>Sezione</th>
                                            <th>Domanda</th>
                                            <th>Quota Pagata</th>
                                            <th>Modifica</th>
                                            <th>Approva</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach(App\User::where('status', 'pending')->orderBy('surname')->get() as $user)
                                            <tr>
                                                <td>{{ $user->name }}</td>
                                                <td>{{ $user->surname }}</td>
                                                <td>{{ $user->username }}</td>
                                                <td>{{ $user->section ? $user->section->city : '' }}</td>
                                                <td>{{ $user->request_at }}</td>
                                                <td>{{ $user->fees()->count() > 0 ? 'SI' : 'NO' }}</td>
                                                <td><a href="{{ route('user.edit', $user->id) }}"><span class="oi oi-pencil"></span></a></td>
                                                <td><input type="checkbox" name="approved[]" value="{{ $user->id }}"></td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                                <div class="form-group row">
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary">Salva</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    @endif

    <div class="row mt-3">
        <div class="col-md-12">
            <table class="table">
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Cognome</th>
                        <th>Nickname</th>
                        <th>Sezione</th>

                        @if($currentuser->hasRole('admin'))
                            <th>Quota</th>
                            <th>Stato</th>
                            <th>Modifica</th>
                        @endif
                    </tr>
                </thead>
                <tbody>
                    @foreach($objects as $user)
                        <tr class="{{ in_array($user->status, ['expelled', 'dropped']) ? 'hidden' : '' }}">
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->surname }}</td>
                            <td>{{ $user->expelled_at ? 'ESPULSO' : $user->username }}</td>
                            <td>{{ $user->section ? $user->section->city : '' }}</td>

                            @if($currentuser->hasRole('admin'))
                                <td>{{ $user->fees()->max('year') }}</td>
                                <td>{{ $user->human_status }}</td>
                                <td><a href="{{ route('user.edit', $user->id) }}"><span class="oi oi-pencil"></span></a></td>
                            @endif
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
